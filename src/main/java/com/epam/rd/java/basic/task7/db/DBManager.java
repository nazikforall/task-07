package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private Connection connection;

	private static final String MESSAGE_CANNOT_PUT_USER_INTO_TEAMS = "Can't put user into teams";

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance=new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			connection= DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		String query = "SELECT id, login FROM users";
		try (Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String name = rs.getString("login");
				int id = rs.getInt("id");
				User user = new User();
				user.setId(id);
				user.setLogin(name);
				userList.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Users not found",e);
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		String sql="INSERT INTO users (login) VALUES (?)";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
			connection.setAutoCommit(false);
			preparedStatement.setString(1, user.getLogin());
			boolean b = preparedStatement.executeUpdate() > 0;
			connection.commit();
			connection.setAutoCommit(true);
			return b;

		} catch (SQLException e) {
			throw new DBException("Not insert user",e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		String collect = Arrays.stream(users).map(user -> "'" + user.getLogin() + "'").collect(Collectors.joining(","));
		String sql="DELETE FROM users WHERE login IN (" + collect + ")";
		try	(Statement preparedStatement=connection.createStatement()) {
			return preparedStatement.executeUpdate(sql)>0;
		} catch (SQLException e) {
			throw new DBException("Can't delete users",e);
		}
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		String sql = "SELECT id, login FROM users WHERE login=?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1,login);
			ResultSet rs = stmt.executeQuery();
			while ((rs.next())) {
				String name = rs.getString("login");
				int id = rs.getInt("id");
				user.setId(id);
				user.setLogin(name);
			}
		} catch (SQLException e) {
			throw new DBException("User not found",e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team=new Team();
		String sql = "SELECT id, name FROM teams WHERE name=?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1,name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				String name1 = rs.getString("name");
				int id = rs.getInt("id");
				team.setId(id);
				team.setName(name1);
			}
		} catch (SQLException e) {
			throw new DBException("Team not found",e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();
		String sql = "SELECT id, name FROM teams";
		try (Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				Team team=new Team();
				team.setId(id);
				team.setName(name);
				teamList.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Teams not found",e);
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql="INSERT INTO teams (name) VALUES (?)";
		try	(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			connection.setAutoCommit(false);
			preparedStatement.setString(1, team.getName());
			int rows = preparedStatement.executeUpdate();
			connection.commit();
			connection.setAutoCommit(true);
			return rows>0;

		} catch (SQLException e) {
			throw new DBException("Not insert team",e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		User foundUser = this.getUser(user.getLogin());

		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DBException(MESSAGE_CANNOT_PUT_USER_INTO_TEAMS, e);
		}
		int rows = 0;
		for (Team t : teams) {
			Team team = this.getTeam(t.getName());
			try (Statement statement=connection.createStatement()){
				String format = String.format("INSERT INTO users_teams (user_id,team_id) VALUES (%s,%s)", foundUser.getId(), team.getId());
				rows = statement.executeUpdate(format);
			} catch (SQLException ex) {
				try {
					connection.rollback();
					throw new DBException(MESSAGE_CANNOT_PUT_USER_INTO_TEAMS, ex);
				} catch (SQLException e) {
					throw new DBException(MESSAGE_CANNOT_PUT_USER_INTO_TEAMS, e);
				}
			}
		}

		try {
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException(MESSAGE_CANNOT_PUT_USER_INTO_TEAMS, e);
			}
		}
		return rows > 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> listTeam=new ArrayList<>();
		String sql= "SELECT t.id, t.name FROM users u " +
				"JOIN users_teams ut ON u.id=ut.user_id " +
				"JOIN teams t ON t.id = ut.team_id " +
				"WHERE u.login='"+user.getLogin()+"'";
		try (Statement preparedStatement=connection.createStatement()) {
			ResultSet resultSet = preparedStatement.executeQuery(sql);
			while (resultSet.next()){
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				Team team=new Team();
				team.setName(name);
				team.setId(id);
				listTeam.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Not get UserTeams",e);
		}
		return listTeam;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Team foundTeam = getTeam(team.getName());
		String selectSql = "SELECT * FROM users_teams WHERE team_id = ?";
		String sql="DELETE FROM users_teams WHERE team_id = ?";
		String sql1="DELETE FROM teams WHERE id = ?";
		String sql2="DELETE FROM users WHERE id = ?";
		try(PreparedStatement statement  = connection.prepareStatement(selectSql);
			PreparedStatement preparedStatement=connection.prepareStatement(sql);
			PreparedStatement preparedStatement1=connection.prepareStatement(sql1);
			PreparedStatement preparedStatement2=connection.prepareStatement(sql2)) {
			int userId = 0;
			statement.setInt(1, foundTeam.getId());
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				userId = rs.getInt(1);
			}

			connection.setAutoCommit(false);

			Map<Integer, Integer> allTeams = this.findAllTeams(userId);

			preparedStatement1.setInt(1, foundTeam.getId());
			preparedStatement1.executeUpdate();

			if (allTeams.size() == 1) {
				preparedStatement2.setInt(1, userId);
				preparedStatement2.executeUpdate();
			}

			preparedStatement.setInt(1, foundTeam.getId());
			preparedStatement.executeUpdate();
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("Can't delete team",e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql="UPDATE teams SET name = ? WHERE id = ?";
		try (PreparedStatement preparedStatement=connection.prepareStatement(sql)){
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2,getTeam(team.getOldValue()).getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Team not updated",e);
		}
		return false;
	}

	public Map<Integer, Integer> findAllTeams(int userId) throws DBException {
		Map<Integer, Integer> map = new TreeMap<>();
		String sql = "SELECT user_id, team_id FROM users_teams WHERE user_id = " + userId;
		try (Statement stmt = connection.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int teamId1 = rs.getInt("team_id");
				int userId1 = rs.getInt("user_id");
				map.put(teamId1, userId1);
			}
		} catch (SQLException e) {
			throw new DBException("Teams not found",e);
		}
		return map;
	}
}