package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}
	@Override
	public String toString(){
		return getLogin();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj.getClass()!=this.getClass())return false;
		return ((User) obj).getLogin().equals(this.login);
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		User u =new User();
		u.setLogin(login);
		u.setId(0);
		return u;
	}

}
